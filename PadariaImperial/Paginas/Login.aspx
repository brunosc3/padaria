﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Paginas_Login_Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Padaria Imperial</title>
    <style type="text/css">
        header{
            background-color: #00C162;
            background-attachment: fixed;
            background-position: center;
            margin-top: 0px;
            text-align: center;
        }
        .auto-style1 {
            height: 150px;
            margin-top: 0px;
            text-align: left;
        }
        menu{

        }
        .auto-style3 {
            height: 20px;
            text-align: justify;
        }
        nav{
            margin-left: 0px;
            text-align: center;
            background-color: #00C162;
        }
        .auto-style6 {
            width: 150px;
            height: 120px;
            float: none;
            margin: 10px;
        }
        </style>
</head>
    <body style="margin-left: 0px; margin-right: 0px; margin-top: 0px";>
    <form id="form1" runat="server">
        <header id="Topo" class="auto-style1">
            <img src="SupImperial.jpeg" class="auto-style6" style="border: thin solid #FFFFFF;" />
            </header>
        <nav>
            <br/>
            <asp:Label ID="Label1" runat="server" Text="Login"></asp:Label>
                <br />
                <br />
            <asp:Label ID="lblNome" runat="server" Text="Nome"></asp:Label>
                <br />
            <asp:TextBox ID="txtNome" runat="server"></asp:TextBox>
             <br />
            <asp:Label ID="lblSenha" runat="server" Text="Senha"></asp:Label>
            <br />
            <asp:TextBox ID="txtSenha" runat="server" type="password"></asp:TextBox>
            <br />
             <br />
            <asp:Button ID="btnEntrar" runat="server" Text="Entrar" OnClick="btnEntrar_Click" />
            <br />
            <asp:Label ID="lblMensagem" runat="server" Text=""></asp:Label>
            <br />
        </nav>
        </form>
</body>
</html>
