﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AlterarProduto.aspx.cs" Inherits="Paginas_PaginasBalconista_AlterarProduto" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Padaria Imperial</title>
    <style type="text/css">
        header{
            background-color: #00C162;
            background-attachment: fixed;
            background-position: center;
            margin-top: 0px;
            text-align: center;
        }
        .auto-style1 {
            height: 150px;
            margin-top: 0px;
            text-align: left;
        }
        .auto-style6 {
            width: 150px;
            height: 120px;
            float: none;
            margin: 10px;
        }
        .auto-style3 {
            height: 20px;
            text-align: justify;
        }
        nav{
            margin-left: 0px;
        }
        .auto-style7 {
            text-align: left;
            height: 320px;
            width: 34%;
        }
        .auto-style4 {
            width: 960px;
            height: 373px;
            text-align: left;
        }
        .auto-style6 {
            width: 150px;
            height: 120px;
            float: none;
            margin: 10px;
        }
		#esquerda{
			width: 50%;
			float: left;
			
		}
		#direita{
			width: 50%;
			float: right;
			
		}
        .auto-style7 {
            height: 320px;
            width: 34%;
        }
        .auto-style8 {
            width: 43%;
            height: 339px;
            text-align: center;
        }
        
        .auto-style9 {
            text-align: center;
        }
        
        </style>
</head>
<body style="margin-left: 0px; margin-right: 0px; margin-top: 0px";>
    <form id="form1" runat="server">
        <header id="Topo" class="auto-style1" style="background-color: #00C162; background-attachment: fixed; background-position: center">
            <img src="../SupImperial.jpeg" class="auto-style6" style="border: thin solid #FFFFFF;" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblTitulo0" runat="server" ForeColor="#003300"></asp:Label>
&nbsp;<asp:Button ID="Button1" runat="server" BackColor="White" ForeColor="#006600" Height="21px" OnClick="lbSair_Click" Text="Sair" Width="37px" />
            </header>
        <nav id="menu" class="auto-style3" style="border-color: #00C162; border-width: medium; border-top-style: solid; border-right-style: none; border-bottom-style: solid; border-left-style: none;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Menu
        <br />
        </nav>
        <header class="auto-style4" style="background-color: #FFFFFF">
		<div id="esquerda" class="auto-style7">
            <br />
&nbsp;&nbsp;&nbsp;
            <asp:Label ID="lblVizualizar" runat="server" Text="Vizualizar" ForeColor="#003300"></asp:Label>
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:HyperLink ID="HL1" runat="server" ForeColor="#006600" NavigateUrl="~/Paginas/PaginasBalconista/ListarMateriaPrima.aspx">Estoque </asp:HyperLink>
		    <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:HyperLink ID="HL2" runat="server" ForeColor="#006600" NavigateUrl="~/Paginas/PaginasBalconista/ListarProduto.aspx">Produtos</asp:HyperLink>
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:HyperLink ID="HL3" runat="server" ForeColor="#006600" NavigateUrl="~/Paginas/PaginasBalconista/ListarPedido.aspx">Pedidos</asp:HyperLink>
            <br />
            <br />
            &nbsp;&nbsp;&nbsp;
            <asp:Label ID="lblRealizar" runat="server" Text="Realizar" ForeColor="#003300"></asp:Label>
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:HyperLink ID="HL7" runat="server" ForeColor="#006600" NavigateUrl="~/Paginas/PaginasBalconista/CadastrarPedido.aspx">Pedidos</asp:HyperLink>
            <br />
		</div>
            <div id="direita" class="auto-style8">
        <div class="auto-style9">
            <br />
            <asp:Label ID="lblTitulo" runat="server" Text="ALTERAR PRODUTO"></asp:Label>
            <br />
            <br />
            <asp:Label ID="lblNome" runat="server" Text="Nome:"></asp:Label>
            <br />
            <asp:TextBox ID="txtNome" runat="server" Width="265px"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="btnSalvarAlteracao" runat="server" Text="Salvar Alteração" OnClick="btnSalvarAlteracao_Click" />
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <br />
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="lblMensagem" runat="server" Text=""></asp:Label>
            <br />
        </div>
        </div>
            </header>
    </form>
</body>
</html>
