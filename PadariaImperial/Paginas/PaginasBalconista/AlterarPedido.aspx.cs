﻿using PadariaImperial.Classes;
using PadariaImperial.Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_PaginasBalconista_AlterarPedido : System.Web.UI.Page
{
    private bool IsBalconista(int tipo)
    {
        bool retorno = false;
        if (tipo == 1)
        {
            retorno = true;
        }
        return retorno;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            PedidoBD bd = new PedidoBD();
            Pedido pedido = bd.Select(Convert.ToInt32(Session["ID"]));
            txtNome.Text = pedido.Nome;
            txtQuantidade.Text = pedido.Quantidade.ToString();
            txtBalconista.Text = pedido.Balconista;
            txtCod.Text = pedido.Produto_Codigo.ToString();
        }
        {
            int codigo = Convert.ToInt32(Session["CODIGO"]);
            FuncionarioBD bd = new FuncionarioBD();
            Funcionario funcionario = bd.Select(codigo);
            if (IsBalconista(funcionario.Tipo))
            {
                lblTitulo0.Text = "Conectado como: " + funcionario.Nome;
            }
            else
            {
                Response.Redirect("../Erro/AcessoNegado.aspx");
            }
        }

    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        {
            PedidoBD bd = new PedidoBD();
            Pedido pedido = bd.Select(Convert.ToInt32(Session["ID"]));
            pedido.Nome = txtNome.Text;
            pedido.Quantidade = Convert.ToDouble(txtQuantidade.Text);
            pedido.Balconista = txtBalconista.Text;
            pedido.Produto_Codigo = Convert.ToInt32(txtCod.Text);



            if (bd.Update(pedido))
            {
                lblMensagem.Text = "Alteração de pedido realizada.";
                txtNome.Focus();
            }
            else
            {
                lblMensagem.Text = "Não foi possível alterar.";
            }

        }
    }
    protected void lbSair_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Session.Clear();
        Session.RemoveAll();
        Response.Redirect("../Login.aspx");
    }
}