﻿using PadariaImperial.Classes;
using PadariaImperial.Persistencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_PaginasBalconista_CadastrarPedido : System.Web.UI.Page
{
    private bool IsBalconista(int tipo)
    {
        bool retorno = false;
        if (tipo == 1)
        {
            retorno = true;
        }
        return retorno;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            Carregarddl();
        {
            int codigo = Convert.ToInt32(Session["CODIGO"]);
            FuncionarioBD bd = new FuncionarioBD();
            Funcionario funcionario = bd.Select(codigo);
            if (!IsBalconista(funcionario.Tipo))
            {
                Response.Redirect("../Erro/AcessoNegado.aspx");

            }
            else
            {
                lblTitulo0.Text = "Conectado como: " + funcionario.Nome;
            }
        }
    }

    private void Carregarddl()
    {
        ProdutoBD p = new ProdutoBD();
        DataSet ds = p.SelectAll();
        ddlProdutos.DataSource = ds.Tables[0].DefaultView;
        ddlProdutos.DataTextField = "pro_nome";
        ddlProdutos.DataValueField = "pro_codigo";
        ddlProdutos.DataBind();
        ddlProdutos.Items.Insert(0, new ListItem("Selecione", "0"));
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        Pedido pedido = new Pedido();
        pedido.Nome = Convert.ToString(ddlProdutos.SelectedItem.Text);
        pedido.Quantidade = Convert.ToDouble(txtQuantidade.Text);
        pedido.Balconista = txtBalconista.Text;
        pedido.Produto_Codigo = Convert.ToInt32(txtCod.Text);
        PedidoBD bd = new PedidoBD();
        if (bd.Insert(pedido))
        {
            ProdutoMateriaPrimaBD produtoMateriaPrimaBD = new ProdutoMateriaPrimaBD();
            DataSet ds = produtoMateriaPrimaBD.GetMateriaPrimaByProduto(Convert.ToInt32(ddlProdutos.SelectedItem.Value));
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                DataRow dr = ds.Tables[0].Rows[i];
                int qtde = Convert.ToInt32(dr["pmp_quantidade"]);
                int mat_id = Convert.ToInt32(dr["mat_codigo"]);
                //DEBITAR
                MateriaPrimaBD materiaPrimaBD = new MateriaPrimaBD();
                materiaPrimaBD.Debitar(qtde, mat_id);
            }

            lblMensagem.Text = "Pedido cadastrado";
            txtQuantidade.Text = "";
            txtBalconista.Text = "";
            txtCod.Focus();
        }
        else
        {
            lblMensagem.Text = "Erro ao cadastrar.";
        }

    }
    protected void lbSair_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Session.Clear();
        Session.RemoveAll();
        Response.Redirect("../Login.aspx");
    }
}