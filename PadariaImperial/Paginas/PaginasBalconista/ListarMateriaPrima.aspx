﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ListarMateriaPrima.aspx.cs" Inherits="Paginas_PaginasBalconista_ListarMateriaPrima" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Padaria Imperial</title>
    <style type="text/css">
        header{
            background-color: #00C162;
            background-attachment: fixed;
            background-position: center;
            margin-top: 0px;
            text-align: center;
        }
        .auto-style1 {
            height: 150px;
            margin-top: 0px;
            text-align: left;
        }
        menu{

        }
        .auto-style3 {
            height: 20px;
            text-align: justify;
        }
        nav{
            margin-left: 0px;
        }
        .auto-style6 {
            width: 150px;
            height: 120px;
            float: none;
            margin: 10px;
        }
        #esquerda{
			width: 50%;
			float: left;
			
		}
        #direita{
			width: 10%;
			float: right;
			
		}
        .auto-style7 {
            text-align: left;
            width: 142px;
        }
        .auto-style8 {
            text-align: center;
            margin-left: 440px;
        }
        </style>
</head>
<body style="margin-left: 0px; margin-right: 0px; margin-top: 0px";>
    <form id="form1" runat="server">
        <header id="Topo" class="auto-style1">
            <img src="../SupImperial.jpeg" class="auto-style6" style="border: thin solid #FFFFFF;" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblTitulo0" runat="server" ForeColor="#003300"></asp:Label>
&nbsp;<asp:Button ID="Button1" runat="server" BackColor="White" ForeColor="#006600" Height="21px" OnClick="lbSair_Click" Text="Sair" Width="37px" />
            </header>
        <nav id="menu" class="auto-style3" style="border-color: #00C162; border-width: medium; border-top-style: solid; border-right-style: none; border-bottom-style: solid; border-left-style: none;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Menu
        <br />
        </nav>
        <header class="auto-style4" style="background-color: #FFFFFF">
		<div id="esquerda" class="auto-style7" style="width: auto">
            <br />
&nbsp;&nbsp;&nbsp;
            <asp:Label ID="lblVizualizar" runat="server" Text="Vizualizar" ForeColor="#003300"></asp:Label>
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:HyperLink ID="HL1" runat="server" ForeColor="#006600" NavigateUrl="~/Paginas/PaginasBalconista/ListarMateriaPrima.aspx">Estoque </asp:HyperLink>
		    <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:HyperLink ID="HL2" runat="server" ForeColor="#006600" NavigateUrl="~/Paginas/PaginasBalconista/ListarProduto.aspx">Produtos</asp:HyperLink>
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:HyperLink ID="HL3" runat="server" ForeColor="#006600" NavigateUrl="~/Paginas/PaginasBalconista/ListarPedido.aspx">Pedidos</asp:HyperLink>
            <br />
            <br />
            &nbsp;&nbsp;&nbsp;
            <asp:Label ID="lblRealizar" runat="server" Text="Realizar" ForeColor="#003300"></asp:Label>
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:HyperLink ID="HL7" runat="server" ForeColor="#006600" NavigateUrl="~/Paginas/PaginasBalconista/CadastrarPedido.aspx">Pedidos</asp:HyperLink>
            <br />
		</div>
        <div class="auto-style8">
            <br />
            <asp:GridView ID="GridView1" runat="server" OnRowCommand="GridView1_RowCommand" AutoGenerateColumns="False" >
                <Columns>
                    <asp:BoundField DataField="mtp_codigo" HeaderText="Código" />
                    <asp:BoundField DataField="mtp_nome" HeaderText="Nome" />
                    <asp:BoundField DataField="mtp_qtdmin" HeaderText="Quantidade Mínima " />
                    <asp:BoundField DataField="mtp_fornecedor" HeaderText="Fornecedor" />
                    <asp:BoundField DataField="mtp_saldo" HeaderText="Saldo" />
                    </Columns>
            </asp:GridView>
        </div>
            </header>
    </form>
</body>
</html>
