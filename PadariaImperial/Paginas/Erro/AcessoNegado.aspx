﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AcessoNegado.aspx.cs" Inherits="Paginas_Erro_AcessoNegado" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="auto-style1">
            <asp:Label ID="Label" runat="server" Text="Acesso Negado"></asp:Label>
            <br />
            <asp:HyperLink ID="HyperLink" runat="server" NavigateUrl="~/Paginas/Login.aspx">Voltar para a tela de Login</asp:HyperLink>
        </div>
    </form>
</body>
</html>
