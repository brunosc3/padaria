﻿using FATEC;
using PadariaImperial.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PadariaImperial.Persistencia
{
    /// <summary>
    /// Descrição resumida de FuncionarioBD
    /// </summary>
    public class FuncionarioBD
    {
        public Funcionario Autentica(string nome, string senha)
        {
            Funcionario obj = null;
            System.Data.IDbConnection objConexao;
            System.Data.IDbCommand objCommand;
            System.Data.IDataReader objDataReader;
            objConexao = Mapped.Connection();
            objCommand = Mapped.Command("SELECT * FROM tbl_usuario WHERE usu_nome=?nome and usu_senha=?senha", objConexao);

            objCommand.Parameters.Add(Mapped.Parameter("?nome", nome));
            objCommand.Parameters.Add(Mapped.Parameter("?senha", senha));
            objDataReader = objCommand.ExecuteReader();
            while (objDataReader.Read())
            {
                obj = new Funcionario();
                obj.Codigo = Convert.ToInt32(objDataReader["usu_codigo"]);
                obj.Nome = Convert.ToString(objDataReader["usu_nome"]);
                obj.Tipo = Convert.ToInt32(objDataReader["usu_tipo"]);
            }
            objDataReader.Close();
            objConexao.Close();
            objCommand.Dispose();
            objConexao.Dispose();
            objDataReader.Dispose();
            return obj;
        }
        public Funcionario Select(int codigo)
        {
            Funcionario obj = null;
            System.Data.IDbConnection objConexao;
            System.Data.IDbCommand objCommand;
            System.Data.IDataReader objDataReader;
            objConexao = Mapped.Connection();
            objCommand = Mapped.Command("SELECT * FROM tbl_usuario WHERE usu_codigo=?codigo", objConexao);
            objCommand.Parameters.Add(Mapped.Parameter("?codigo", codigo));
            objDataReader = objCommand.ExecuteReader();
            while (objDataReader.Read())
            {
                obj = new Funcionario();
                obj.Codigo = Convert.ToInt32(objDataReader["usu_codigo"]);
                obj.Nome = Convert.ToString(objDataReader["usu_nome"]);
                obj.Tipo = Convert.ToInt32(objDataReader["usu_tipo"]);
            }
            objDataReader.Close();
            objConexao.Close();
            objCommand.Dispose();
            objConexao.Dispose();
            objDataReader.Dispose();
            return obj;
        }
        public FuncionarioBD()
        {
            //
            // TODO: Adicionar lógica do construtor aqui
            //
        }
    }
}