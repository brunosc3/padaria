﻿using FATEC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de ProdutoMateriaPrimaBD
/// </summary>
public class ProdutoMateriaPrimaBD
{

    public DataSet GetMateriaPrimaByProduto(int produtoID)
    {
        DataSet ds = new DataSet();
        System.Data.IDbConnection objConexao;
        System.Data.IDbCommand objCommand;
        System.Data.IDataAdapter objDataAdapter;
        objConexao = Mapped.Connection();
        objCommand = Mapped.Command("SELECT * FROM padaria.tbl_produtomateriaprima where pro_codigo=?produto;", objConexao);
        objCommand.Parameters.Add(Mapped.Parameter("?produto", produtoID));
        objDataAdapter = Mapped.Adapter(objCommand);
        objDataAdapter.Fill(ds);
        objConexao.Close();
        objCommand.Dispose();
        objConexao.Dispose();
        return ds;
    }


    public ProdutoMateriaPrimaBD()
    {
        
    }
}