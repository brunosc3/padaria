﻿using FATEC;
using PadariaImperial.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace PadariaImperial.Persistencia
{
    /// <summary>
    /// Descrição resumida de PedidoBD
    /// </summary>
    public class PedidoBD
    {
        //métodos
        //insert
        public bool Insert(Pedido pedido)
        {
            System.Data.IDbConnection objConexao;
            System.Data.IDbCommand objCommand;
            string sql = "INSERT INTO tbl_pedido(ped_nome, ped_quantidade, ped_balconista, pro_cod) VALUES (?nome, ?quantidade, ?balconista, ?cod)";
            objConexao = Mapped.Connection();
            objCommand = Mapped.Command(sql, objConexao);
            objCommand.Parameters.Add(Mapped.Parameter("?nome", pedido.Nome));
            objCommand.Parameters.Add(Mapped.Parameter("?quantidade", pedido.Quantidade));
            objCommand.Parameters.Add(Mapped.Parameter("?balconista", pedido.Balconista));
            objCommand.Parameters.Add(Mapped.Parameter("?cod", pedido.Produto_Codigo));
            objCommand.ExecuteNonQuery();
            objConexao.Close();
            objCommand.Dispose();
            objConexao.Dispose();
            return true;
        }

        //selectall
        public DataSet SelectAll()
        {
            DataSet ds = new DataSet();
            System.Data.IDbConnection objConexao;
            System.Data.IDbCommand objCommand;
            System.Data.IDataAdapter objDataAdapter;
            objConexao = Mapped.Connection();
            objCommand = Mapped.Command("SELECT * FROM tbl_pedido", objConexao);
            objDataAdapter = Mapped.Adapter(objCommand);
            objDataAdapter.Fill(ds);
            objConexao.Close();
            objCommand.Dispose();
            objConexao.Dispose();
            return ds;
        }
        //select
        public Pedido Select(int id)
        {
            Pedido obj = null;
            System.Data.IDbConnection objConexao;
            System.Data.IDbCommand objCommand;
            System.Data.IDataReader objDataReader;
            objConexao = Mapped.Connection();
            objCommand = Mapped.Command("SELECT * FROM tbl_pedido WHERE ped_codigo=?codigo", objConexao);
            objCommand.Parameters.Add(Mapped.Parameter("?codigo", id));
            objDataReader = objCommand.ExecuteReader();
            while (objDataReader.Read())
            {
                obj = new Pedido();
                obj.Codigo = Convert.ToInt32(objDataReader["ped_codigo"]);
                obj.Nome = Convert.ToString(objDataReader["ped_nome"]);
                obj.Quantidade = Convert.ToDouble(objDataReader["ped_quantidade"]);
                obj.Balconista = Convert.ToString(objDataReader["ped_balconista"]);
                obj.Produto_Codigo = Convert.ToInt32(objDataReader["pro_cod"]);
            }
            objDataReader.Close();
            objConexao.Close();
            objCommand.Dispose();
            objConexao.Dispose();
            objDataReader.Dispose();
            return obj;
        }

        //update
        public bool Update(Pedido pedido)
        {
            System.Data.IDbConnection objConexao;
            System.Data.IDbCommand objCommand;
            string sql = "UPDATE tbl_pedido SET ped_nome=?nome, ped_quantidade=?quantidade, ped_balconista=?balconista, pro_cod=?cod WHERE ped_codigo=?codigo";
            objConexao = Mapped.Connection();
            objCommand = Mapped.Command(sql, objConexao);
            objCommand.Parameters.Add(Mapped.Parameter("?codigo", pedido.Codigo));
            objCommand.Parameters.Add(Mapped.Parameter("?nome", pedido.Nome));
            objCommand.Parameters.Add(Mapped.Parameter("?quantidade", pedido.Quantidade));
            objCommand.Parameters.Add(Mapped.Parameter("?balconista", pedido.Balconista));
            objCommand.Parameters.Add(Mapped.Parameter("?cod", pedido.Produto_Codigo));
            objCommand.ExecuteNonQuery();
            objConexao.Close();
            objCommand.Dispose();
            objConexao.Dispose();
            return true;
        }
        //delete
        public bool Delete(int id)
        {
            System.Data.IDbConnection objConexao;
            System.Data.IDbCommand objCommand;
            string sql = "DELETE FROM tbl_pedido WHERE ped_codigo=?codigo";
            objConexao = Mapped.Connection();
            objCommand = Mapped.Command(sql, objConexao);
            objCommand.Parameters.Add(Mapped.Parameter("?codigo", id));

            objCommand.ExecuteNonQuery();
            objConexao.Close();
            objCommand.Dispose();
            objConexao.Dispose();
            return true;
        }
        //construtor
        public PedidoBD()
        {
            //
            // TODO: Adicionar lógica do construtor aqui
            //
        }
    }
}