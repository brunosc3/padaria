﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PadariaImperial.Classes
{
    /// <summary>
    /// Descrição resumida de Produto
    /// </summary>
    public class Produto
    {
        //propriedades
        public int Codigo { get; set; }
        public string Nome { get; set; }

        public Produto()
        {
            //
            // TODO: Adicionar lógica do construtor aqui
            //
        }
    }
}