﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PadariaImperial.Classes
{
    /// <summary>
    /// Descrição resumida de MateriaPrima
    /// </summary>
    public class MateriaPrima
    {
        //propriedades
        public int Codigo { get; set; }
        public string Nome { get; set; }
        public double QuantidadeMinima { get; set; }
        public string Fornecedor { get; set; }
        public double Saldo { get; set; }

        public MateriaPrima()
        {
            //
            // TODO: Adicionar lógica do construtor aqui
            //
        }
    }
}