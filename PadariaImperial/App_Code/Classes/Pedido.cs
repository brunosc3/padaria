﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PadariaImperial.Classes
{
    /// <summary>
    /// Descrição resumida de Pedido
    /// </summary>
    public class Pedido
    {
        public int Codigo { get; set; }
        public string Nome { get; set; }
        public double Quantidade { get; set; }
        public string Balconista { get; set; }
        public int Produto_Codigo { get; set; }

        public Pedido()
        {
            //
            // TODO: Adicionar lógica do construtor aqui
            //
        }
    }
}